package com.litmusworld.intern.framework;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;
import org.apache.log4j.Logger;


public class ConfigurationProperties {

	final static Logger logger = Logger.getLogger(ConfigurationProperties.class);
	private static ConfigurationProperties s_Instance;
	private String propFileName = null;
	private Properties properties;

	public static class ConfigPropertyKeys {
		public static final String SQL_DB_DRIVER_CLASSNAME = "jdbc_drivername";
		public static final String SQL_DB_URL = "jdbc_url";
		public static final String SQL_DB_DATABASE_NAME="jdbc_database";
		public static final String SQL_DB_USERNAME = "jdbc_username";
		public static final String SQL_DB_PASSWORD = "jdbc_password";
		public static final String SQL_QUERY_CREATE="table_definitions";
		

	}

	private ConfigurationProperties() {
		propFileName = System.getProperty("PROP_FILE");
		logger.info("path for properties file : " + propFileName);

		InputStream inputStream;
		try {
			inputStream = new FileInputStream(propFileName);

			properties = new Properties();
			properties.load(inputStream);

		} catch (FileNotFoundException e) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			e.printStackTrace(ps);
			logger.error(baos.toString());
		} catch (IOException e) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			e.printStackTrace(ps);
			logger.error(baos.toString());
		} catch (Exception e) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			e.printStackTrace(ps);
			logger.error(baos.toString());
		}
	}

	public static ConfigurationProperties getInstance() {
		if (s_Instance == null) {
			synchronized (ConfigurationProperties.class) {
				if (s_Instance == null) {
					s_Instance = new ConfigurationProperties();
				}
			}
		}

		return s_Instance;
	}

	public String getSqlDriverClassName() {
		if (this.properties.getProperty(ConfigPropertyKeys.SQL_DB_DRIVER_CLASSNAME) != null) {
			return this.properties.getProperty(ConfigPropertyKeys.SQL_DB_DRIVER_CLASSNAME).toString();
		} else {
			return null;
		}
	}
	
	public String getSqlDbUrl() {
		if (this.properties.getProperty(ConfigPropertyKeys.SQL_DB_URL) != null&&!this.properties.getProperty(ConfigPropertyKeys.SQL_DB_URL).isEmpty()) {
			return this.properties.getProperty(ConfigPropertyKeys.SQL_DB_URL).toString();
		} else {
			return null;
		}
	}

	
	public String getSqlUsername() {
		if (this.properties.getProperty(ConfigPropertyKeys.SQL_DB_USERNAME) != null&&!this.properties.getProperty(ConfigPropertyKeys.SQL_DB_USERNAME).isEmpty()) {
			return this.properties.getProperty(ConfigPropertyKeys.SQL_DB_USERNAME).toString();
		} else {
			return null;
		}
	}

	
	public String getSqlPassword() {
		if (this.properties.getProperty(ConfigPropertyKeys.SQL_DB_PASSWORD) != null&&!this.properties.getProperty(ConfigPropertyKeys.SQL_DB_PASSWORD).isEmpty()) {
			return this.properties.getProperty(ConfigPropertyKeys.SQL_DB_PASSWORD).toString();
		} else {
			return null;
		}
	}

	public String getSqlDatabaseName() {
		if (this.properties.getProperty(ConfigPropertyKeys.SQL_DB_DATABASE_NAME) != null&&!this.properties.getProperty(ConfigPropertyKeys.SQL_DB_DATABASE_NAME).isEmpty()) {
			return this.properties.getProperty(ConfigPropertyKeys.SQL_DB_DATABASE_NAME).toString();
		} else {
			return null;
		}
	}
	public String getSqlQuery() {
		if (this.properties.getProperty(ConfigPropertyKeys.SQL_QUERY_CREATE) != null&&!this.properties.getProperty(ConfigPropertyKeys.SQL_QUERY_CREATE).isEmpty()) {
			return this.properties.getProperty(ConfigPropertyKeys.SQL_QUERY_CREATE).toString();
		} else {
			return null;
		}
	}
	

}
