package com.litmusworld.intern.framework;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DbManager {
	
    static Logger logger = Logger.getLogger(DbManager.class);
    String getpath = null;
    private static String url=null;    
    private static String driverName=null;   
    private static String username =null;   
    private static String password =null;
    private static String databaseName =null;
    private static Connection con;
    private static DbManager instance = null;
    
	public  DbManager () { 
		getpath = System.getProperty("PROP_FILE");
		
		System.out.println("PATH: "+ getpath);
		try {
			FileInputStream fis = new FileInputStream(getpath);
			Properties p = new Properties();
			p.load(fis);
			driverName = (String) p.get("jdbc_drivername");
			url = (String) p.get("jdbc_url");
			username = (String) p.get("jdbc_username");
			password = (String) p.get("jdbc_password");
			databaseName=(String) p.get("jdbc_database");
		} catch (Exception e) {
		  e.getMessage();
		  System.out.println("Invalid file path");
		}
	}
	
    public static  DbManager getInstance() {
        if(instance == null) {
            try {
                instance = new DbManager();
                System.out.println("Object created");
            } catch (Exception e) {
              e.printStackTrace();
			}
        }
        return instance;
    }
    
    
   public static Connection getConnection() {
        try {
        	logger.info("DriverName: "+ driverName);
            Class.forName(driverName);
            try {
            	
              con = DriverManager.getConnection(url);
               //con = DriverManager.getConnection("jdbc:mysql://localhost:3306/litmus12?user=root&password=litmus@123");
               logger.info("Database connected");
            } catch (Exception ex) {
          	   ex.printStackTrace();
               logger.info("Failed to create the database connection."); 
            }
         } catch (ClassNotFoundException ex) {
        	  ex.printStackTrace();
             logger.error(ex);
        	 logger.info("Driver not found."); 
         }
          return con;
       }
    
   public static String  closeConnection() {
    	String msg="";
    	if(con!=null) {
    		try {
				con.close();
				msg="Connection hasbeen closed ";
			} catch (SQLException e) {
				logger.info("Exceptin from Closing the connection :"+e);
				e.printStackTrace();
			 }
    	  }else {
    		msg="Connection is not even opened, its null so need not to close ";
    	  }
    	return msg;
      }
    
}
