package com.lts.intern.InternNew;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.litmusworld.intern.framework.DbManager;
import java.sql.PreparedStatement;

/**
 * 1.Create a table by java code and read table definition from config file.
 * 2.Read data from csv file and insert into above table(CSV file path take from
 * cmd prompt).
 */

public class NewClass {
	private static Statement statement;
	private static Connection con;
	private static PreparedStatement Pstatement;
	final static Logger logger = Logger.getLogger(NewClass.class);

	public static void main(String[] args) throws Exception {

		String Createquery = ReadTableDefinition();
		createTable(Createquery);
		if (args.length == 0) {
			System.out.println("Enter the filepath");

		}
		Scanner sc = new Scanner(System.in);
		String csvFile = sc.nextLine();
		ReadAndInsert(csvFile);

	}

	private static String ReadTableDefinition() {

		String values = "";
		String myTableName = null;
		String s = null;
		String query = com.litmusworld.intern.framework.ConfigurationProperties.getInstance().getSqlQuery();
		JSONArray arrObj = new JSONArray(query);
		for (int i = 0; i < arrObj.length(); i++) {
			LinkedHashMap<String, String> myMap = new LinkedHashMap<String, String>();
			String[] keyValue = null;
			String csvheader = arrObj.get(i) + "";
			logger.info(",,,,,,,,,,,,,,,,,,," + csvheader);
			String[] pairs = csvheader.split(",");
			for (String s1 : pairs) {
				keyValue = s1.split(":");
				myMap.put(keyValue[0], keyValue[1]);

			}
			logger.info("LinkedHashMap: " + myMap);

			for (Map.Entry m : myMap.entrySet()) {
				if (values.length() == 0) {
					values += m.getValue();

				} else {
					values += m.getValue();
				}

			}

			System.out.println("........" + values);

			s = values.replaceAll("\"", " ").trim();
			logger.info(s);
			if (s.endsWith("}")) {
				s = s.substring(0, s.length() - 1);
			}

		}
		return s;

	}

	public static void createTable(String query) {

		String myTableName = "CREATE TABLE InternNew (" + query.replaceAll("}", ",") + ")" + ";";
		logger.info(myTableName);
		try {
			DbManager.getInstance();
			con = DbManager.getConnection();
			statement = con.createStatement();
			statement.executeUpdate(myTableName);
			logger.info("Table created successfully");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Table already exists");
		}
	}

	public static void ReadAndInsert(String csvFile) throws Exception {
		String id1 = null;
		String name = null;
		String address = null;
		int batchSize = 20;

		try {
			// String csvFile = "C:\\Users\\baby.bai\\Downloads\\Demo.csv";

			String sql = "INSERT INTO InternNew (idNo,name,address) VALUES (?, ?, ?)";
			DbManager.getInstance();
			Pstatement = con.prepareStatement(sql);

			BufferedReader lineReader = new BufferedReader(new FileReader(csvFile));
			String lineText = null;

			lineReader.readLine(); // skip header line
			int count = 0;
			while ((lineText = lineReader.readLine()) != null) {
				String[] data = lineText.split(",");

				id1 = data[0];
				name = data[1];
				address = data[2];

				int id = Integer.parseInt(id1);
				Pstatement.setInt(1, id);
				Pstatement.setString(2, name);
				Pstatement.setString(3, address);

				Pstatement.addBatch();

				/*
				 * if (count % batchSize == 0) { Pstatement .executeBatch(); }
				 */
				count++;
			}
			lineReader.close();
			Pstatement.executeBatch();
		} finally {
			DbManager.getInstance().closeConnection();
		}

	}

}
